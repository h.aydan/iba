import React,{useState,useEffect} from 'react';
import { GiRoundStar } from "react-icons/gi";
import {Button} from './Button';
import  {Cart} from './Cart'


export const Shop=({name,price,id,addToCart,addToFavorites,image,favoriteAdded,setFavoriteAdded})=>{



     return(
           <div className='shop'> 
                   <div key={id} className='shop-item'>
                      <img className='shop-item-img' src={image}/>
                      <div className='shop-item-desc'>
                        <h3>{name}</h3>
                        <p>Price</p>
                        <h2>{price}$</h2>
                      <Button 
                        name = { name }
                        image = { image }
                        price = { price } 
                        addToCart={addToCart}
                        addToFavorites={addToFavorites}
                        favoriteAdded={favoriteAdded}
                        setFavoriteAdded={setFavoriteAdded}
                        />
                      </div>
                  </div>
                </div>

      );
}