import React,{useState,useEffect} from 'react';
import { GiRoundStar } from "react-icons/gi";


export const Button=({ addToFavorites,addToCart})=>{

const [buttonColor,setButtonColor]=useState(false);
const handleColor = () =>{
       setButtonColor(e=>!e);
       addToFavorites();
}

   return(
       <div className='button-holder'>
       <button 
                  className='add-btn'
                  onClick={ addToCart }>
                   Add to cart
                    </button>
        <button 
      className={buttonColor ? "star-btn-true": "star-btn-false"} 
      onClick={handleColor}>
          <GiRoundStar/>
      </button>
      </div>
  );
}
