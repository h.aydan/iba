import React,{useState,useEffect} from 'react';
import { GiRoundStar } from "react-icons/gi";
import {Header} from './Header';
import {Modal} from'./Modal';
import {Shop} from './Shop';
import { Cart} from'./Cart';
import {Favorites} from './Favorites';
import {Footer} from './Footer'


function App() {
const [data,setdata]=useState([]);
const [modal,setModal]=useState(false);
const [cart, setCart] = useState([]);
const [favorites, setFavorites]=useState([]);


const getData=async()=>{
  const req = await fetch(`http://localhost:3000/products`);
  const json = await req.json();
  setdata(json);
};
useEffect(() => {
 getData();
}, [])

const addToFavorites = (name, price, id) => {
  if(!isNaN(price)) {
    const isAvailable = favorites.find((item) => id === item.id);

    if(isAvailable){
      setFavorites(favorites => favorites.map(item => {
        if(item.id === id) {
          return{
            ...item,
            liked : item.count + 1
          }
        }
        return item;
      }))
    } else {
      setFavorites(favorites => [...favorites, {
        name,
        price,
        id,
        liked: 1
      }])
    }
  } 
}



const addToCart = (name, price, id) => {
  if(!isNaN(price)) {
    const isAvailable = cart.find((item) => id === item.id);

    if(isAvailable){
      setCart(cart => cart.map(item => {
        if(item.id === id) {
          return{
            ...item,
            count : item.count + 1
          }
        }
        return item;
      }))
    } else {
      setCart(cart => [...cart, {
        name,
        price,
        id,
        count: 1
      }])
    }
  } 
  setModal(e=>{
    if(e==false){
      return (!e)
    } else {
      return (e)
    }
  });

}
  return (
    <div className="App">
      <Header/> 
      <div className='basket'>
     <Cart 
          cart= {cart}
          setCart={setCart}/>
          
    <Favorites
             favorites={favorites}
            setFavorites={setFavorites}
    />
    </div>
     <div className="products-list">
     {data.map(({name,price,id,image})=>(
      <Shop
        key={id}
        name={name}
        price={price}
        image={image}
        addToCart={() => addToCart(name, price, id)}
        addToFavorites={() => addToFavorites(name, price, id)}
        />
     ))}
   </div>

    {modal&&<Modal modal={modal} setModal={setModal} />}
    <Footer/>
    </div>
  );
}

export default App;
