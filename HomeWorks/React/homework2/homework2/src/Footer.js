import React from 'react'
import { FaFacebookF,
         FaInstagram,
         FaYoutube,
         FaTwitter } from "react-icons/fa";


export const Footer=()=>{
   return(
        <div className='footer'>
           <div className='footer-logo'><h3>&copy; Idric corporation inc. 2020</h3></div>
           <div className='footer-social-media-holder'>
                             <button className='footer-social-media-item'><FaFacebookF/></button>
                             <button className='footer-social-media-item'><FaInstagram/></button>
                             <button className='footer-social-media-item'><FaYoutube/></button>
                             <button className='footer-social-media-item'><FaTwitter/></button>

            </div> 
        </div>

   );
}