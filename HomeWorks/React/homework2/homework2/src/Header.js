import React from 'react'
import { FaRobot } from "react-icons/fa";
export const Header=()=>{
    return(
         <div className='header'>
             <h3 className='header-logo'><FaRobot/>  Idrix</h3>
             <h2>Wellcome to our Robo-Shop!</h2> 
             <div className='header-input-holder' > 
                 <input type='text' className='header-input'/>
                 <button className='header-input-btn'>Search</button>   
             </div>
                  
         </div>
    );
}