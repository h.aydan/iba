import React from 'react';
import { FaRegStar } from "react-icons/fa";

export const Favorites = ({ favorites, setFavorites}) => {

          
    const removeFromFavorites=( id , liked ) => {
         if(liked  > 1) {
                  setFavorites(favorites => favorites.map(item=>{
                 if(item.id === id){
                  return{
                      ...item,
                      leked: item.count - 1
                  }
                 }     
                 return item      
            }))
          } else {
                  setFavorites( favorites => favorites.filter(item => item.id !== id ))
          }
    }
    return(
            <div className="favorites">
                 <h3 className='favorites-header'> <FaRegStar/> Favorites </h3>
                  {favorites.map(({name , price , id , liked}) => (
                     <div key={id} className='favorites-item'>
                          <h4>{name}</h4>
                          <i>price:{price}$</i>
                          <br/>
                          <button
                           onClick={()=>removeFromFavorites(id, liked )}
                          > ✖ </button>
                     </div>
                  ))}             
            </div>          
    )
}
