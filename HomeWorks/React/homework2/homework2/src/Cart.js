import React from 'react';

export const Cart = ({ cart, setCart}) => {

          
    const removeFromCart=( id , count ) => {
         if(count > 1) {
            setCart(cart => cart.map(item=>{
                 if(item.id === id){
                  return{
                      ...item,
                      count: item.count - 1
                  }
                 }     
                 return item      
            }))
          } else {
               setCart( cart => cart.filter(item => item.id !== id ))
          }
    }
    return(
            <div className="cart">
                 <h3>Total: 
                     {cart.reduce((total,{ price , count })=>total + price * count ,0)}
                    </h3>  
                  {cart.map(({image, name , price , id , count }) => (
                     <div key={id} className='cart-item'>
                          <img src={image}/>
                          <h4>{name}</h4>
                           <p >Quantity:{count}</p>
                          <i>price:{price}$</i>
                          <br/>
                          <button
                           onClick={()=>removeFromCart(id, count)}
                          > ✖ </button>
                     </div>
                  ))}             
            </div>          
    )
}
