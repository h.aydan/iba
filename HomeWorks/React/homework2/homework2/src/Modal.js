import React,{useState,useEffect} from 'react';

export const Modal=({modal,setModal})=>{
    const handleModal = () =>{
          setModal(e=>!e);
      }
    return(
        <div className='modal-holder'>
               <header className='modal-header'>
                    <h2 className='modal-header-heding'>Sucsess</h2>
                    <button onClick={ handleModal} className='modal-header-btn' > ✖ </button>
                </header>           
               <p>✔ You have added an item to your card</p>
               <button onClick={ handleModal } className='modal-confirm-btn'> Confirm </button>

        </div>
     )      
}
