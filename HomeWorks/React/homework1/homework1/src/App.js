import React,{useState} from 'react';
import './App.css';
import Button from "./components/Button";
import Modal from "./components/Modal";
import styles from "./styles.sass"



function App() {

        const [modalWindowOne, setModalWindowOne]=useState(false);
        const [modalWindowTwo, setModalWindowTwo]=useState(false);

        const toggleWindowOne=()=>setModalWindowOne(m=>!m);
        const toggleWindowTwo=()=>setModalWindowTwo(m=>!m);



        return ( <div className = "App" >

        <Button text = { 'Open First Modal' }
                backgroundColor = { 'red' }
                onClick={toggleWindowOne}/>

        <Button text = { 'Open Second Modal' }
                backgroundColor = { 'green' }
                onClick={toggleWindowTwo}/>

        {modalWindowOne && ( <Modal
            header={'Do you want to delete this file?'}
            closeIcon={true}
            backgroundColor={"#e74c3c"}
            text={'Once you delete this file, it won’t be possible to undo this action. ' +
            'Are you sure you want to delete it?'}
            close={toggleWindowOne}
            actions={[
                <Button
                    text={"Ok"}
                    backgroundColor={"rgba(0 , 0, 0, 0.2)"}
                    onClick={toggleWindowOne}/>,
                <Button
                    text={"Cancel"}
                    backgroundColor={"rgba(0 , 0, 0, 0.2)"}
                    onClick={toggleWindowOne}/>
            ]
            }
        />)
        }

        {modalWindowTwo && (
            <Modal
                header={'Do you want to save this file?'}
                   closeIcon={true}
                   backgroundColor={"#839900"}
                   text={'Once you save this file, it won’t be possible to undo this action.' +
                   'Are you sure you want to save it?'}
                   close={toggleWindowTwo}
                   actions={
                       [
                           <Button
                               text={"Save"}
                               backgroundColor={"rgba(0 , 0, 0, 0.2)"}
                               onClick={toggleWindowTwo}/>,
                           <Button
                               text={"Cancel"}
                               backgroundColor={"rgba(0 , 0, 0, 0.2)"}
                               onClick={toggleWindowTwo}/>
                       ]
                   }
            />)
        }

    </div>
        );
        }


        export default App;