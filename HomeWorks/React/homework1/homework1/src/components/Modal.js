import React from 'react'
import PropTypes from 'prop-types';

const  Modal = ({header,closeIcon,actions,text,close,backgroundColor})=>{

    return(
        <div className='modal'
             style = {{ backgroundColor}}
        >
            <header className='modal-header'>
                {header}
                { {closeIcon} && <button onClick={close} className='close-btn'> ╳ </button> }
            </header>
            <p className='modal-text'>{text}</p>
            <div className='btn-holder'>
                {actions}
            </div>

        </div>
    );
};


Modal.propTypes= {
   header:PropTypes.string,
    closeIcon:PropTypes.bool,
    actions:PropTypes.element,
    text:PropTypes.string,
    close:PropTypes.func,
};


export default Modal