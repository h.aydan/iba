//section tree tabs
let tab  = document.querySelectorAll(".section-tree-tabs-table-item");
let tabContent=document.querySelectorAll(".tabs-content-holder");
console.log(tab);
console.log(tabContent);
for(let i=0;i<tab.length;i++){
    tab[i].addEventListener('click',()=> {

        for (let j=0;j<tab.length;j++){
            tab[j].classList.remove("active");
            tabContent[j].style.display="none";
        }
        tab[i].classList.add("active");
        tabContent[i].style.display="flex";
    })
}

// section 5 js code

const filterTitlesContainer =  document.getElementById('filter-titles');
const projects = document.querySelectorAll('.project-item');

filterTitlesContainer.addEventListener('click', (e) => {
    if(e.target.classList.contains('filter-title')) {
        const title = e.target;
        const type = title.dataset.filterby || 'project-item';
        const isActive = title.classList.contains('active');

        if(!isActive) {
            document.querySelector('.filter-title.active').classList.remove('active');
            title.classList.add('active');

            filterByClassName(projects, type);
        }
        // console.log(isActive);
    }
});

function filterByClassName(elements, className) {
    for(let element of elements) {
        element.hidden = !element.classList.contains(className);
    }
}

