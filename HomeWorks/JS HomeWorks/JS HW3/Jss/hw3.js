function createNewUser(name , surname)
{
 let newUser={
    firstName:name,
    lastName:surname,
   getLogin:function(){
          let lowName=newUser.firstName.toLowerCase();
          let lowLastName=newUser.lastName.toLowerCase();
          let firstChar=lowName[0];
          return firstChar+lowLastName;
      }
  };
 return newUser;
}
console.log(createNewUser("Freddie","Mercury").getLogin());


//Theoretical question:
// Objects are usually created to represent entities of the real world, like users, orders and so on.And, in the real world,
// a user can act: select something from the shopping cart, login, logout etc.For this we have Methods.JavaScript methods
// are actions that can be performed on objects.Basically A method is  a function inside a object.In js we have some
// Built-In Methods such as "toUpperCase()" , "some()" , "filter()", "map()" and so on ready to use.You can also creat
// your own method by simply using function definition and define whatever action you want to do with them.
