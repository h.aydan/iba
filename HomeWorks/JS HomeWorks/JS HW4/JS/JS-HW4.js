function filterBy(arr,dataType){
    let answerArr=[];
    arr.forEach(function (element){
       if(typeof element !== dataType){
           answerArr.push(element);
       }
   });
   return answerArr;
}
let arr= ['hello', 'world', 23, '23', null];
console.log(filterBy(arr,'string'));

//Teoretical question:
// The forEach() method calls a function once for each element in an array.The provided function may perform any kind of
// operation on the elements of the given array.The argument to this function is another function that defines the condition
// to be checked for each element of the array.It always returns undefined.We can use forEach() method with arrays,maps
// and sets.Before Array.forEach(), looping through an array involved using for loops.Even tho both the for loop and
// forEach method allows you to loop over the array,forEach has better readability - has a much nicer syntax.(the more
// readable code is always superior after all)than the regular for loop.The use of a variable for the item makes working
// with array data a lot easier.And it has a big advantage as it has fewer off-by-one errors.
